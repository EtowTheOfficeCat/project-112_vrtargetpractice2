# VRTarget Practice II

**Hey EtowTheOfficeCat here! Going to present you a new project here based of one of my earlier ones!**

![](https://media.giphy.com/media/jUiko9b4A2rfNAB3eh/giphy.gif)

:grey_exclamation: **game is not finished, still working ont it** :grey_exclamation:

I created this Gameproject to learn and try out new things in VR. I made it on the basis of my midterm exam project of the same name (https://gitlab.com/EtowTheOfficeCat/project-102_vr-target-practice). But decided to change and evolve it more with all the new things that I have learned on the way. Base mechanic in this game is shooting targets that fly passt the players from one or several spawning areas towards one or several goals. The player has to shoot them down before they arrive at their destinations only being allowed to have a certain amount of mistakes. To do so the player has a weapon (bow) with several different types of amunition. He will need to use his ammunition reserves wisely sincet they are not be infinit. The ammunition though will permit the player to use certain strategies to hit serveral targets at once, maximising the use of his weapon. Goal is to  finish all the waves of a level. Plan is to have several different types of levels with different challenges and possibilities for the player to try and play.

**Preview Video of the Game:**

https://youtu.be/3T1sbhhvxUQ (13.11.2019)

**Pictures of the Game:**

![](https://i.imgur.com/idDl8lW.png)

working on the game UI, here the arrow shop- that the player can use between waves. (15.11.2019)

![](https://i.imgur.com/7vZGv6M.png) ![](https://i.imgur.com/iIqRvl9.jpg)

Game won screen, lets the player look at the beautifull destruction!(13.11.19)

![](https://media.giphy.com/media/SS2HecfqJ3raygwbti/giphy.gif)

Added a new ammunition type, makes the target expload but debris remain floating, other targets can hit the debris and in turn get destroyed. Also added a dissolve shader for the debris.(27.10.19)

All the assets used are simple Unity assets/cubes in cobination with different materials and light settings as well as post-processing. I did inspire myself from the Beat Saber environment but my main goal here was to create a simple yet elegant environment that also serves the purpouse of the game. 

**Programming the Game:**

Programming in this project has so far been easy, mostly combining things learned thus far and improving on them, like creating simple bow mechanic but adding the possibility to change ammunition or using Nvidia Blast to make destructable targerts etc...

**still to do (14.11.19)**

- FIX/The Bow feels a little bit sluggish since I added the weaponswitch.
- Add a menu/pointer to the game
- Add new levels to the game
- Add comments to all scripts
- Add a Pool for enemy spawning. 