﻿using UnityEngine;
using TMPro;

public class UIScript : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI BlueBowText;
    [SerializeField] TextMeshProUGUI RedBowText;
    [SerializeField] TextMeshProUGUI GreenBowText;
    [SerializeField] TextMeshProUGUI LifesText;

    [SerializeField] Bow BlueBow;
    [SerializeField] Bow RedBow;
    [SerializeField] Bow GreenBow;

    void Start()
    {
        
    }

   
    void Update()
    {
       
        BlueBowText.text = "" + BlueBow.BowAmmo;
        RedBowText.text =  "" +RedBow.BowAmmo;
        GreenBowText.text = "" + GreenBow.BowAmmo;
        LifesText.text = "Lifes: " + GAME.lifes;
    }
}
