﻿using UnityEngine;
 using Valve.VR;

public class SteamInput : MonoBehaviour
{
    
    public Bow Bow = null;
    public SteamVR_Behaviour_Pose Pose = null;
    public SteamVR_Action_Boolean PullAction = null;
    

    private void Update()
    {
        if (PullAction.GetStateDown(Pose.inputSource))
            Bow.Pull(Pose.gameObject.transform);

        if (PullAction.GetStateUp(Pose.inputSource))
            Bow.Release();

    }
    
}
