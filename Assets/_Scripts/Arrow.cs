﻿using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float Speed = 2000.0f;
    public Transform Tip = null;

    private Rigidbody rb = null;
    private bool isStopped = true;
    private Vector3 lastPosition = Vector3.zero;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (isStopped)
            return;

        rb.MoveRotation(Quaternion.LookRotation(rb.velocity, transform.up));

        lastPosition = Tip.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
            

    }

    private void Stop()
    {
        isStopped = true;

        rb.isKinematic = true;
        rb.useGravity = false;
    }

    public void Fire(float pullValue)
    {
        isStopped = false;
        transform.parent = null;

        rb.isKinematic = false;
        rb.useGravity = true;
        rb.AddForce(transform.forward * (pullValue * Speed));

        Destroy(gameObject, 15.0f);
    }
}
