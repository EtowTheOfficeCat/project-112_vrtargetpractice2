﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GAME : MonoBehaviour
{
    public enum GameState { SPAWNING, WAITING, COUNTDOWN, GAMEOVER, GAMEWON };
    [SerializeField] private Vector3 spawnCenter;
    [SerializeField] private Vector3 spawnSize;
    [SerializeField] private TextMeshProUGUI EnemyCount;
    
    [System.Serializable]
    public class wave
    {
        public string name;
        public GameObject enemy;
        public int count;
        public float rate;
    }

    public wave[] waves;
    public float timeBetweenVaves = 5f;
    public float waveCountDown;
    public TextMeshProUGUI WaveText;

    private int nextWave = 0;
    private float searchCountDown = 1;
    private int wavecounter;
    private int numberOfWaves;
    public static int lifes;
    private GameState state = GameState.COUNTDOWN;

    private void Start()
    {
        waveCountDown = timeBetweenVaves;
        numberOfWaves = waves.Length;
        lifes = 5;
    }

    private void Update()
    {
        wavecounter = nextWave + 1;
        if (state == GameState.COUNTDOWN)
        {
            //wave text
            WaveText.text = ("Wave " + wavecounter + "/" +  numberOfWaves + " starts in:" + waveCountDown.ToString("00"));
        }

        if (state == GameState.SPAWNING)
        {
            WaveText.text = ("");
        }
        

        if (state == GameState.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
            }

            else
            {
                return;
            }
        }

        if (state == GameState.GAMEWON)
        {
            WaveText.text = (" Game Won");
        }

        if(lifes == 0)
        {
            WaveText.text = (" Game Over");
            Time.timeScale = 0f;
        }
        

        if (waveCountDown <= 0)
        {
            if(state != GameState.SPAWNING)
            {
                //star spawning wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        state = GameState.COUNTDOWN;
        waveCountDown = timeBetweenVaves;
        
        
        if(nextWave + 1 > waves.Length - 1)
        {
            state = GameState.GAMEWON;
            Time.timeScale = 0f;
            //completed all waves
        }
        else
        {
            nextWave++;
        }
        
    }

    bool EnemyIsAlive()
    {
        //checking if any enemies are still alive, if not start next wave
        searchCountDown -= Time.deltaTime;

            if(searchCountDown <= 0f)
            {
                searchCountDown = 1f;
                if(GameObject.FindGameObjectWithTag("Enemy") == null)
                {
                return false;
                }
            }
        return true;


    }
   IEnumerator SpawnWave (wave _wave)
    {
        //Spawning 
        state = GameState.SPAWNING;
        
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            
            yield return new WaitForSeconds(1 / _wave.rate);
        }
        state = GameState.WAITING;

        yield break;
    }

    void  SpawnEnemy(GameObject _enemy)
    {
        //Spawning Enemy in a random position within given space(cube)
        Vector3 pos = transform.localPosition + spawnCenter + new Vector3(
           Random.Range(-spawnSize.x / 2, spawnSize.x / 2),
           Random.Range(-spawnSize.y / 2, spawnSize.y / 2),
           Random.Range(-spawnSize.z / 2, spawnSize.z / 2));

        Instantiate(_enemy, pos, Quaternion.identity);

    }

    private void OnDrawGizmosSelected()
    {
        //Drawing spawning space for scene view
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(transform.localPosition + spawnCenter, spawnSize);
    }
}
