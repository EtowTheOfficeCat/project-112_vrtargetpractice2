﻿using System.Collections;
using UnityEngine;

public class Bow : MonoBehaviour
{
    [Header("Assets")]
    public GameObject Arrow;
    public int BowAmmo;
    private GameObject currentPrefab;

    [Header("Bow")]
    public float GrabThreshold = 0.15f;
    public Transform AStart = null;
    public Transform End = null;
    public Transform Socket = null;
    private int ArrowUsed = 1;

    private Transform pullingHand = null;
    private Arrow currentArrow = null;
    private Animator animator = null;

    private float pullValue = 0.0f;
    [Header("ArrowAmunition")]

    

    [SerializeField] private AudioClip FireArrowClip;
    [SerializeField] private AudioSource BowsoundSource;

    private bool first = true;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        BowsoundSource.clip = FireArrowClip;
        
    }

    private void Start()
    {
        currentPrefab = Arrow;
        
    }

    private void Update()
    {
        if(first == true)
        {
            StartCoroutine(CreateArrow(0.1f, Arrow));
            first = false;
        }
        if (!pullingHand || !currentArrow)
            return;

        pullValue = CalculatePull(pullingHand);
        pullValue = Mathf.Clamp(pullValue, 0.0f, 1.0f);

        animator.SetFloat("Blend", pullValue);

    }

    

    private float CalculatePull(Transform pullHand)
    {

        Vector3 direction = End.position - AStart.position;
        float magnitude = direction.magnitude;

        direction.Normalize();
        Vector3 difference = pullHand.position - AStart.position;


        return Vector3.Dot(difference, direction) / magnitude;
    }

    private IEnumerator CreateArrow(float waitTime, GameObject arrowPrefab)
    {
       
        yield return new WaitForSeconds(waitTime);
        
        GameObject arrowObject = Instantiate(arrowPrefab, Socket);

        arrowObject.transform.localPosition = new Vector3(0, 0, 0.425f);
        arrowObject.transform.localEulerAngles = Vector3.zero;

        currentArrow = arrowObject.GetComponent<Arrow>();

    }

    public void Pull(Transform hand)
    {
        float distance = Vector3.Distance(hand.position, AStart.position);

        if (distance > GrabThreshold)
            return;

        pullingHand = hand;
    }

    public void Release()
    {
        if (pullValue > 0.25f)
            FireArrow();

        pullingHand = null;

        pullValue = 0.0f;
        animator.SetFloat("Blend", 0);

        

        if (BowAmmo == 0 )
        {
            return;
        }
        if (!currentArrow)
            StartCoroutine(CreateArrow(0.25f, currentPrefab));

    }

    private void FireArrow()
    {
        currentArrow.Fire(pullValue);
        if (currentPrefab == Arrow) { BowAmmo -= 1; }
       
        currentArrow = null;
        BowsoundSource.Play();

    }
}
