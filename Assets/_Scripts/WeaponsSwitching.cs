﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class WeaponsSwitching : MonoBehaviour
{
    public int selectedweapon = 0;
    public SteamVR_Behaviour_Pose Pose = null;
    public SteamVR_Action_Boolean ChangeArrow = null;

    void Start()
    {
        SelectWeapon();
    }

    
    
    void Update()
    {
        int previousSelectedWeapon = selectedweapon;
        if (ChangeArrow.GetStateDown(Pose.inputSource))
        {
            if (selectedweapon >= transform.childCount - 1)
                selectedweapon = 0;
            else
                selectedweapon++;
        }
            

        if (previousSelectedWeapon != selectedweapon)
        {
            SelectWeapon();
        }
    }

    

    public void SelectWeapon()
    {
        int i = 0;
        foreach(Transform weapon in transform)
        {
            if (i == selectedweapon)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            i++;
        }
    }
}
