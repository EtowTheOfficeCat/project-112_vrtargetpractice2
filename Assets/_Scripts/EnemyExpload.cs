﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExpload : MonoBehaviour
{
    [SerializeField] private AudioClip expload;
    [SerializeField] private AudioSource soundSource;

    [SerializeField] private float explosionForce = 100;
    [SerializeField] private float explosionRadius = 10;

    public Material dissolveMat;
    public Material dissolveMat2;
    private float DissolveTimer = 0;
    private float timeToDissolve = 1;
    public Renderer[] rendInChildreen;

    private float timer = 0;
   

    private void Awake()
    {
        timer = 0;
        soundSource.clip = expload;
        dissolveMat.SetFloat("Vector1_636EE82F", 0);
        dissolveMat2.SetFloat("Vector1_636EE82F", 0);
    }
    void Start()
    {
        

        soundSource.Play();
        foreach (Transform chunk in transform)
        {
            var rb = chunk.GetComponent<Rigidbody>();

            rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);

        }
    }

    private void Update()
    {
        float desTime;
        desTime = timer += Time.deltaTime;
        if (desTime >= 7)
        {
            StartCoroutine(Dissolve());
            
        }
    }

    public IEnumerator Dissolve()
    {
        float time = DissolveTimer += Time.deltaTime;
        rendInChildreen = GetComponentsInChildren<Renderer>();
        foreach (Renderer rend in rendInChildreen)
        {
            dissolveMat = rend.materials[0];
            dissolveMat2 = rend.materials[1];
            dissolveMat.SetFloat("Vector1_636EE82F", time / timeToDissolve);
            dissolveMat2.SetFloat("Vector1_636EE82F", time / timeToDissolve);
        }
        yield return new WaitForSeconds(1.5f);
        
        Destroy(gameObject);
    }

}
