﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private GameObject fracturedPrefab;
    [SerializeField] private GameObject fracturedPrefabExplosive;
    [SerializeField] private GameObject fracturedPrefabFloating;



    [SerializeField] private GameObject goal;
    [SerializeField] private float speed;
     private Vector3 directionToTarget;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        goal = GameObject.Find("EnemyGoal");
        //give the enemy its target goal
        
    }

    private void Update()
    {
        MoveToTarget();
    }

    public void MoveToTarget()
    {
        directionToTarget = (goal.transform.position - transform.position).normalized;
        rb.velocity = new Vector3(directionToTarget.x * speed, directionToTarget.y * speed, directionToTarget.z * speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        //depending ammunition type hit, giving the right prefab for enemy explosion
        if (other.CompareTag("RegularArrow"))
        {
            Instantiate(fracturedPrefab, transform.position, Random.rotation);
            Destroy(transform.root.gameObject);
        }

        if (other.CompareTag("ExplosiveArrow"))
        {
            Instantiate(fracturedPrefabExplosive, transform.position, Random.rotation);
            Destroy(transform.root.gameObject);
        }

        if (other.CompareTag("ExplosiveFloatingArrow"))
        {
            Instantiate(fracturedPrefabFloating, transform.position, Random.rotation);
            Destroy(transform.root.gameObject);
        }

        //remove one life each time enemy arrives at its destination
        if (other.CompareTag("EnemyGoal"))
        {
            GAME.lifes--;
            Destroy(transform.root.gameObject);
        }


    }



}
